/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package vectnd

import "errors"

// Make2D creates a slice of dimension [r][c].
// In terms of a matrix, this is analogous to an r row by c column matrix.
func Make2D(r, c int) (A [][]float64) {
	A = make([][]float64, r)
	for i := 0; i < r; i++ {
		A[i] = make([]float64, c)
	}
	return A
}

// Get2DColAs1D returns a colmn of a matrix as a vector
// Assumes that c is a valid column
func Get2DColAs1D(A [][]float64, c int) (result []float64) {
	result = make([]float64, len(A))
	for i, row := range A {
		result[i] = row[c]
	}
	return result
}

// Make3D creates a slice of dimension [r][c][d].
// In terms of a matrix, this is analogous to an r row by c column by d depth matrix.
func Make3D(r, c, d int) (A [][][]float64) {
	A = make([][][]float64, r)
	for i := 0; i < r; i++ {
		A[i] = make([][]float64, c)
		for j := 0; j < c; j++ {
			A[i][j] = make([]float64, d)
		}
	}
	return A
}

// CheckMatrix checks to see if a matrix has equal length vectors and is square or not
func CheckMatrix(A [][]float64) (eqVec, square bool) {
	width := len(A[0])
	square = width == len(A)
	for _, vec := range A {
		if len(vec) != width {
			return false, square
		}
	}
	return true, square
}

// Transpose2dFloat creates the transpose of a rectangular 2-dimensional slice of type float64 as if it were a transposable matrix.
func Transpose2dFloat(A [][]float64) (B [][]float64, err error) {
	if eqVec, _ := CheckMatrix(A); !eqVec {
		return nil, errors.New("cannot transpose a non-rectangular 2D slice")
	}

	B = Make2D(len(A[0]), len(A))
	for i := 0; i < len(A); i++ {
		for j := 0; j < len(A[i]); j++ {
			B[j][i] = A[i][j]
		}
	}
	return B, nil
}

// ScalerMult1D returns the vector v multiplied by s
func ScalerMult1D(v []float64, s float64) (result []float64) {
	result = make([]float64, len(v))
	for i, num := range v {
		result[i] = s * num
	}
	return result
}

// ScalerMult2D returns the matrix m multiplied by s
func ScalerMult2D(m [][]float64, s float64) (result [][]float64) {
	result = make([][]float64, len(m))
	for i, v := range m {
		result[i] = make([]float64, len(v))
		for j, num := range v {
			result[i][j] = s * num
		}
	}
	return result
}

// CalcxtAsy carries out vector-matrix-vector multiplication for symmetric A (assumes CheckMatrix(A) = true, true, A is symetric, x, y, and both dimensions of A are equal)
func CalcxtAsy(x []float64, As [][]float64, y []float64) (result float64) {
	n := len(As)
	d := make([]float64, n)
	for i := 0; i < n; i++ { /*  d = A y  */
		for j := 0; j < n; j++ { //  A in upper triangle only
			if i <= j {
				d[i] += As[i][j] * y[j]
			} else {
				d[i] += As[j][i] * y[j]
			}
		}
	}
	for i := 0; i < n; i++ { /*  xAy = x' A y  */
		result += x[i] * d[i]
	}
	return result
}

// MultAy carries out matrix-vector multiplication for A (assumes len(A[0]) == len(y), CheckMatrix(A) = true, NA)
func MultAy(A [][]float64, y []float64) (result []float64) {
	result = make([]float64, len(A))
	for i := 0; i < len(A); i++ { /*  d = A y  */
		for j := 0; j < len(A[0]); j++ {
			result[i] += A[i][j] * y[j]
		}
	}
	return result
}

// Add1D adds two vectors (assumes len(x) == len(y))
func Add1D(x, y []float64) (result []float64) {
	result = make([]float64, len(x))
	for i := range x {
		result[i] = x[i] + y[i]
	}
	return result
}

// Subtract1D subtracts two vectors x-y (assumes len(x) == len(y))
func Subtract1D(x, y []float64) (result []float64) {
	result = make([]float64, len(x))
	for i := range x {
		result[i] = x[i] - y[i]
	}
	return result
}

// Equal2D determines if A and B have the same number of row, columns, and values
func Equal2D(A, B [][]float64) (rows, cols, vals bool) {
	cols = true
	vals = true
	rows = len(A) == len(B)
	if !rows {
		return rows, false, false
	}
	for i := range A {
		if len(A[i]) != len(B[i]) {
			return rows, false, false
		}
		if vals {
			for j := range A[i] {
				if A[i][j] != A[i][j] {
					vals = false
				}
			}
		}
	}
	return rows, cols, vals
}

// Add2D adds two matricies (assumes Equal2D(A,B) = true, true, either)
func Add2D(A, B [][]float64) (C [][]float64) {
	C = make([][]float64, len(A))
	for i := range A {
		C[i] = make([]float64, len(A[i]))
		for j := range A[i] {
			C[i][j] = A[i][j] + B[i][j]
		}
	}
	return C
}

// Mult2D multiplies two matricies to gether A*B (assumes len(A[0]) == len(B)) C will be C[len(A)][len(B[0])]
func Mult2D(A, B [][]float64) (C [][]float64) {
	C = Make2D(len(A), len(B[0]))
	for arows := 0; arows < len(A); arows++ {
		for bcols := 0; bcols < len(B[0]); bcols++ {
			for inner := 0; inner < len(A[0]); inner++ {
				C[arows][bcols] += A[arows][inner] * B[inner][bcols]
			}
		}
	}
	return C
}
