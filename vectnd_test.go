/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package vectnd

import (
	"testing"
)

func TestMult2D(t *testing.T) {
	a := [][]float64{
		[]float64{1, 2, 3},
		[]float64{4, 5, 6},
		[]float64{7, 8, 9}}
	b := [][]float64{
		[]float64{1, 2, 3},
		[]float64{4, 5, 6},
		[]float64{7, 8, 9}}
	c := [][]float64{
		[]float64{30, 36, 42},
		[]float64{66, 81, 96},
		[]float64{102, 126, 150}}
	ans := Mult2D(a, b)
	if rows, cols, values := Equal2D(ans, c); !(rows && cols && values) {
		t.Errorf("expected:\n%#v\ngot:\n%#v\n", c, ans)
	}
}
